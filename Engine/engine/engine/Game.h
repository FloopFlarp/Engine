#ifndef GAME_H
#define GAME_H
#endif
#include "SDL.h"
//#undef main
#include <iostream>


class Game {

public:
	Game();
	~Game();

	void init(const char* title, int xposition, int yposition, int width, int height, bool fullscreen);
	
	void handleEvents();
	void update();
	void render();
	void clean(); // memory management

	bool running() { return isRunning; }

	static SDL_Renderer *renderer;

private:
	int cnt = 0;
	bool isRunning;
	SDL_Window *window;
};