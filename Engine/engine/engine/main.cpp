#include "Game.h"
#include "SDL.h"
#undef main

Game *game = nullptr;

int main(int argc, const char *argv[])
{
	const int FPS = 60; // Move hardcoded variable to variable for settings
	const int frameDelay = 1000 / FPS; //Maximum time between frames.

	Uint32 frameStart;
	int frameTime;

	game = new Game();
	game->init("FloopEngine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 640, false);

	while (game->running())
	{
		frameStart = SDL_GetTicks();

		game->handleEvents();
		game->update();
		game->render();

		frameTime = SDL_GetTicks() - frameStart;
		if (frameDelay > frameTime)
		{
			SDL_Delay(frameDelay - frameTime);
		}
	}

	game->clean();


	return 0;
}