#include "GameObject.h"
#include "TextureManager.h"

GameObject::GameObject(const char* texturesheet, int x, int y)
{
	objTexture = TextureManager::LoadTexture(texturesheet);

	xpos = x;
	ypos = y;
}

void GameObject::Update()
{
	// Movement and Behavior Code

	xpos++;
	ypos++;

	srcRect.h = 32;
	srcRect.w = 32;
	srcRect.x = 0;
	srcRect.y = 0;

	destRect.x = xpos;
	destRect.y = ypos;

	destRect.w = srcRect.w * 2; //double size of sprite size
	destRect.h = srcRect.h * 2; //ditto

}

void GameObject::Render()
{
	// Pass any render code
	SDL_RenderCopy(Game::renderer, objTexture, &srcRect, &destRect);
}